﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Windows.Media;


namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for DashBoardWindow.xaml
    /// </summary>
    public partial class DashBoardWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        public DashBoardWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void OnHomeBtnClicked(object sender, RoutedEventArgs e)
        {
            HomeWindow hWindow = new HomeWindow();
            hWindow.Show();
            this.Close();
        }
        private void OnProjectBtnClicked(object sender, RoutedEventArgs e)
        {
            ProjectWindow pWindow = new ProjectWindow();
            pWindow.Show();
            this.Close();
        }
        private void OnCurrentBtnClicked(object sender, RoutedEventArgs e)
        {
            CurrentWindow cWindow = new CurrentWindow();
            cWindow.Show();
            this.Close();
        }
        private void OnProductBtnClicked(object sender, RoutedEventArgs e)
        {
            BackLogWindow bWindow = new BackLogWindow();
            bWindow.Show();
            this.Close();
        }

        //OnDashBoardBtnClicked
        private void OnDashBoardBtnClicked(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dWindow = new DashBoardWindow();
            dWindow.Show();
            this.Close();
        }
        private void OnMyWorkClicked(object sender, RoutedEventArgs e)
        {
            MyWorkWindow mWindow = new MyWorkWindow();
            mWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
              //  List<UserStory> userstoryDB = modelContext.GetAllUserStoryBySprintID(1);
                List<UserStory> userstoryDB = modelContext.GetAllUserStoryCurrent();
                ObservableCollection<CustomDashBoard> temp = new ObservableCollection<CustomDashBoard>();
                foreach (var u in userstoryDB)
                {
                    CustomDashBoard cdb = new CustomDashBoard(u);
                    temp.Add(cdb);
                }

                customUserStory = temp;


                //List<CustomDashBoardSprint> sprintdb = modelContext.GetAllTimeBySprintByProjectID(1);
                //ObservableCollection<CustomDashBoardSprint> temp1 = new ObservableCollection<CustomDashBoardSprint>();
                //foreach (var s in sprintdb)
                //{
                //    temp1.Add(s);
                //}

                //customSprintByProject = temp1;

                //List<CustomDashBoardProject> cdbpList = modelContext.GetAllTimeByProject();
                //ObservableCollection<CustomDashBoardProject> temp2 = new ObservableCollection<CustomDashBoardProject>();
                //foreach (var c in cdbpList)
                //{
                //    temp2.Add(c);
                //}

                //customProjectDashBoard = temp2;

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }



        public ObservableCollection<CustomDashBoardProject> customProjectDashBoard
        {
            get { return (ObservableCollection<CustomDashBoardProject>)GetValue(customProjectDashBoardProperty); }
            set { SetValue(customProjectDashBoardProperty, value); }
        }

        // Using a DependencyProperty as the backing store for customProjectDashBoard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty customProjectDashBoardProperty =
            DependencyProperty.Register("customProjectDashBoard", typeof(ObservableCollection<CustomDashBoardProject>), typeof(DashBoardWindow), new PropertyMetadata());




        public ObservableCollection<CustomDashBoardSprint> customSprintByProject
        {
            get { return (ObservableCollection<CustomDashBoardSprint>)GetValue(customSprintByProjectProperty); }
            set { SetValue(customSprintByProjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for customSprintByProject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty customSprintByProjectProperty =
            DependencyProperty.Register("customSprintByProject", typeof(ObservableCollection<CustomDashBoardSprint>), typeof(DashBoardWindow), new PropertyMetadata());




        public ObservableCollection<CustomDashBoard> customUserStory
        {
            get { return (ObservableCollection<CustomDashBoard>)GetValue(dashboardsProperty); }
            set { SetValue(dashboardsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for customUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty dashboardsProperty =
            DependencyProperty.Register("customUserStory", typeof(ObservableCollection<CustomDashBoard>), typeof(DashBoardWindow), new PropertyMetadata());



        public CustomDashBoard SelectedDashBoard
        {
            get { return (CustomDashBoard)GetValue(SelectedDashBoardProperty); }
            set { SetValue(SelectedDashBoardProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedDashBoard.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedDashBoardProperty =
            DependencyProperty.Register("SelectedDashBoard", typeof(CustomDashBoard), typeof(DashBoardWindow), new PropertyMetadata());


        private void DataGrid1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            var dashBoard = row.DataContext as CustomDashBoard;
            if (dashBoard.Percentage < 50)
                row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f44242"));
            else if (dashBoard.Percentage < 80)
                row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f4eb41"));
            else
                row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#41f450"));
        }
        private void DataGrid2_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            var dashBoard = row.DataContext as CustomDashBoardSprint;
            //   MessageBox.Show(dashBoard.Percentage.ToString());
            if (dashBoard != null)
            {
                if (dashBoard.Percentage < 50)
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f44242"));
                else if (dashBoard.Percentage < 100)
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f4eb41"));
                else
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#41f450"));
            }
        }
        private void DataGrid3_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            var dashBoard = row.DataContext as CustomDashBoardProject;
            //   MessageBox.Show(dashBoard.Percentage.ToString());
            if (dashBoard != null)
            {
                if (dashBoard.Percentage < 50)
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f44242"));
                else if (dashBoard.Percentage < 100)
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f4eb41"));
                else
                    row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#41f450"));
            }
        }

    }
    // by team member
    public class CustomDashBoard
    {
        AllModelContext modelContextDashBoard = new AllModelContext();

        public string UserStory { get; set; }
        public int InitialEstimate { get; set; }
        public int ActualTime { get; set; }
        public double Percentage { get; set; }
       // public int Member_ID { get; set; }
        public string Name { get; set; }

        public CustomDashBoard(UserStory u)
        {
            double initial = u.InitialEstimates;
            double actual = u.TotalHours;
            this.InitialEstimate = u.InitialEstimates;
            this.ActualTime = u.TotalHours;
            this.Percentage = Math.Abs(Math.Round((actual-initial)/initial * 100,2));
            if (u.Member_ID != null)
            {
                Name = modelContextDashBoard.GetMemberByID(u.Member_ID.Value).Name;

            }

            UserStory = u.Title;
            
        }
    }

}
