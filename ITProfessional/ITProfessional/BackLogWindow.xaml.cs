﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for BackLogWindow.xaml
    /// </summary>
    public partial class BackLogWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();
        public BackLogWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void OnHomeBtnClicked(object sender, RoutedEventArgs e)
        {
            HomeWindow hWindow = new HomeWindow();
            hWindow.Show();
            this.Close();
        }
        private void OnProjectBtnClicked(object sender, RoutedEventArgs e)
        {
            ProjectWindow pWindow = new ProjectWindow();
            pWindow.Show();
            this.Close();
        }
        private void OnCurrentBtnClicked(object sender, RoutedEventArgs e)
        {
            CurrentWindow cWindow = new CurrentWindow();
            cWindow.Show();
            this.Close();
        }
        private void OnProductBtnClicked(object sender, RoutedEventArgs e)
        {
            BackLogWindow bWindow = new BackLogWindow();
            bWindow.Show();
            this.Close();
        }
        private void OnDashBoardBtnClicked(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dWindow = new DashBoardWindow();
            dWindow.Show();
            this.Close();
        }
        private void OnMyWorkClicked(object sender, RoutedEventArgs e)
        {
            MyWorkWindow mWindow = new MyWorkWindow();
            mWindow.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<UserStory> userstoryDB = modelContext.GetAllUserStoryBacklog();
                // later model.Context.GetAllUserStoryBacklog()
                ObservableCollection<UserStory> temp = new ObservableCollection<UserStory>();
                foreach (var u in userstoryDB)
                {
                  
                    if (u.Member_ID!=null)
                    {
                        u.MemberName = modelContext.GetMemberByID(u.Member_ID.Value).Name;
                    }
                    temp.Add(u);
                }

                UserStories = temp;

                List<UserStory> userstoryDBCurrent = modelContext.GetAllUserStoryCurrent();
                ObservableCollection<UserStory> tempCurrent = new ObservableCollection<UserStory>();
                foreach (var u in userstoryDBCurrent)
                {
                    if (u.Member_ID != null)
                    {
                        u.MemberName = modelContext.GetMemberByID(u.Member_ID.Value).Name;
                        tempCurrent.Add(u);
                    }
                }

                UserStoriesCurrent = tempCurrent;

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }


        public ObservableCollection<UserStory> UserStories
        {
            get { return (ObservableCollection<UserStory>)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("UserStories", typeof(ObservableCollection<UserStory>), typeof(BackLogWindow), new PropertyMetadata());

        public UserStory SelectedUserStory
        {
            get { return (UserStory)GetValue(SelectedUserStoryProperty); }
            set { SetValue(SelectedUserStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryProperty =
            DependencyProperty.Register("SelectedUserStory", typeof(UserStory), typeof(BackLogWindow), new PropertyMetadata());


        //OnAddUserStoryClicked
        private void OnAddUserStoryClicked(object sender, RoutedEventArgs e)
        {
            AddUserStoryWindow aWindow = new AddUserStoryWindow();
            aWindow.ShowDialog();
        }

        ///
        public ObservableCollection<UserStory> UserStoriesCurrent
        {
            get { return (ObservableCollection<UserStory>)GetValue(MyPropertyPropertyCurrent); }
            set { SetValue(MyPropertyPropertyCurrent, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyPropertyCurrent =
            DependencyProperty.Register("UserStoriesCurrent", typeof(ObservableCollection<UserStory>), typeof(BackLogWindow), new PropertyMetadata());

        public UserStory SelectedUserStoryCurrent
        {
            get { return (UserStory)GetValue(SelectedUserStoryPropertyCurrent); }
            set { SetValue(SelectedUserStoryPropertyCurrent, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryPropertyCurrent =
            DependencyProperty.Register("SelectedUserStoryCurrent", typeof(UserStory), typeof(BackLogWindow), new PropertyMetadata());


   
        private void OnUpdateUserStoryClicked(object sender, RoutedEventArgs e)
        {
            //SelectedUserStory
            if (SelectedUserStoryCurrent != null)
            {
                if (SelectedUserStoryCurrent.UserStory_ID > 0)
                {
                    int feedBack = modelContext.UpdateUserStory(SelectedUserStoryCurrent);
                    if (feedBack > 0)
                        MessageBox.Show("Successfuly updated");
                    else
                        MessageBox.Show("Nothing has been changed");
                }
            }
            else
            {
                MessageBox.Show("Please Select a row");
            }
        }
    }

    
}
