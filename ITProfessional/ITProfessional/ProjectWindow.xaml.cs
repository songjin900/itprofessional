﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for ProjectWindow.xaml
    /// </summary>
    /// 
    //OnMyWorkClicked
    public partial class ProjectWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        public ProjectWindow()
        {
            InitializeComponent();
            this.DataContext = this;

        }

        private void OnHomeBtnClicked(object sender, RoutedEventArgs e)
        {
            HomeWindow hWindow = new HomeWindow();
            hWindow.Show();
            this.Close();
        }
        private void OnProjectBtnClicked(object sender, RoutedEventArgs e)
        {
            ProjectWindow pWindow = new ProjectWindow();
            pWindow.Show();
            this.Close();
        }
        private void OnCurrentBtnClicked(object sender, RoutedEventArgs e)
        {
            CurrentWindow cWindow = new CurrentWindow();
            cWindow.Show();
            this.Close();
        }
        private void OnProductBtnClicked(object sender, RoutedEventArgs e)
        {
            BackLogWindow bWindow = new BackLogWindow();
            bWindow.Show();
            this.Close();
        }
        private void OnDashBoardBtnClicked(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dWindow = new DashBoardWindow();
            dWindow.Show();
            this.Close();
        }
        private void OnMyWorkClicked(object sender, RoutedEventArgs e)
        {
            MyWorkWindow mWindow = new MyWorkWindow();
            mWindow.ShowDialog();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Project> projectsDB = modelContext.GetAllProjects();
                ObservableCollection<Project> temp = new ObservableCollection<Project>();
                foreach (var p in projectsDB)
                {
                    temp.Add(p);
                }
             
                Projects = temp;

            }
            catch(Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;

            }
        }


        public ObservableCollection<Project> Projects   
        {
            get { return (ObservableCollection<Project>)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("Projects", typeof(ObservableCollection<Project>), typeof(ProjectWindow), new PropertyMetadata(null));




        public Project SelectedProject
        {
            get { return (Project)GetValue(SelectedProjectProperty); }
            set { SetValue(SelectedProjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedProject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedProjectProperty =
            DependencyProperty.Register("SelectedProject", typeof(Project), typeof(ProjectWindow), new PropertyMetadata());

        private void OnOpenClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedProject != null)
            {
                Project selectedTemp = SelectedProject;

                ProjectDetailWindow pdWindow =  new ProjectDetailWindow(selectedTemp);
                pdWindow.ShowDialog();
                
            }
            else
            {
                MessageBox.Show("Please select project");
            }

        }

        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedProject!=null)
            {
                if (SelectedProject.Project_ID>0)
                {
                    MessageBox.Show("Please unselect a current row and select new row");
                }
                else
                {
                    try
                    {
                        Project newProject = SelectedProject;
                        modelContext.AddProject(newProject);
                        MessageBox.Show("New Project Added");

                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("error: " + ex);
                        throw ex;
                    }
                }

            }
            else
            {
                MessageBox.Show("Please enter information to datagrid");
            }

        }
    }
}
