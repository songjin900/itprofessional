﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for CurrentWindow.xaml
    /// </summary>
    public partial class CurrentWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();
        Sprint sprint = new Sprint();

        //public CurrentWindow(Sprint s)
        //{
        //    InitializeComponent();
        //    sprint = s;

          
        //    this.DataContext = this;
        //}
        public CurrentWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        private void OnHomeBtnClicked(object sender, RoutedEventArgs e)
        {
            HomeWindow hWindow = new HomeWindow();
            hWindow.Show();
            this.Close();
        }
        private void OnProjectBtnClicked(object sender, RoutedEventArgs e)
        {
            ProjectWindow pWindow = new ProjectWindow();
            pWindow.Show();
            this.Close();
        }
        private void OnCurrentBtnClicked(object sender, RoutedEventArgs e)
        {
            CurrentWindow cWindow = new CurrentWindow();
            cWindow.Show();
            this.Close();
        }
        private void OnProductBtnClicked(object sender, RoutedEventArgs e)
        {
            BackLogWindow bWindow = new BackLogWindow();
            bWindow.Show();
            this.Close();
        }
        private void OnDashBoardBtnClicked(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dWindow = new DashBoardWindow();
            dWindow.Show();
            this.Close();
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<UserStory> userstoryDBCurrent = modelContext.GetAllUserStoryCurrent();
                ObservableCollection<UserStory> tempCurrent = new ObservableCollection<UserStory>();
                foreach (var u in userstoryDBCurrent)
                {
                    tempCurrent.Add(u);
                }

                UserStoriesCurrent = tempCurrent;

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }

        public ObservableCollection<UserStory> UserStoriesCurrent
        {
            get { return (ObservableCollection<UserStory>)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("UserStoriesCurrent", typeof(ObservableCollection<UserStory>), typeof(CurrentWindow), new PropertyMetadata());

        public UserStory SelectedUserStoryCurrent
        {
            get { return (UserStory)GetValue(SelectedUserStoryProperty); }
            set { SetValue(SelectedUserStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryProperty =
            DependencyProperty.Register("SelectedUserStoryCurrent", typeof(UserStory), typeof(CurrentWindow), new PropertyMetadata());


        //OnAddUserStoryClicked
        private void OnUpdateUserStoryClicked(object sender, RoutedEventArgs e)
        {
            AddUserStoryWindow aWindow = new AddUserStoryWindow(SelectedUserStoryCurrent, sprint);
            aWindow.Show();
            this.Close();
        }
    }
}
