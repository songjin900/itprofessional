﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for ProjectDetailWindow.xaml
    /// </summary>
    public partial class ProjectDetailWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();
        Project proj = new Project();

        public ProjectDetailWindow(Project p)
        {
            InitializeComponent();
            proj = p;

            tbProjectID.Text = p.Project_ID.ToString();
            tbHours.Text = p.Hours.ToString();
            tbInitialVelocity.Text = p.InitialVelocity.ToString();
            tbPivotalLink.Text = p.PivotalLink.ToString();
            tbProductName.Text = p.ProductName.ToString();
            tbProjectName.Text = p.ProjectName.ToString();
            tbStartDate.Text = p.StartDate.ToString();
            if (p.IsDone == true)
                cbIsDone.IsChecked = true;
            else
                cbIsDone.IsChecked = false;

            this.DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Sprint> sprintDB = modelContext.GetSprintByProjectID(proj.Project_ID);
                ObservableCollection<Sprint> temp = new ObservableCollection<Sprint>();
                foreach (var s in sprintDB)
                {
                    temp.Add(s);
                }

                Sprints = temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }
        //OnSelectClicked
        private void OnSelectClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedSprint!=null)
            {
                try
                {
                    List<UserStory> userstoryDB = modelContext.GetAllUserStoryBySprintID(SelectedSprint.Sprint_ID);
                    ObservableCollection<UserStory> temp = new ObservableCollection<UserStory>();
                    foreach (var u in userstoryDB)
                    {
                        if (u.Member_ID != null)
                        {
                            u.MemberName = modelContext.GetMemberByID(u.Member_ID.Value).Name;

                        }
                        temp.Add(u);
                    }

                    UserStories = temp;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("error in loading up CB " + ex);
                    MessageBox.Show(ex.ToString());
                    throw ex;
                }
            }
            else
            {
                MessageBox.Show("Please Select a Sprint");
            }
            
        }


    

        public ObservableCollection<UserStory> UserStories
        {
            get { return (ObservableCollection<UserStory>)GetValue(USMyPropertyProperty); }
            set { SetValue(USMyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty USMyPropertyProperty =
            DependencyProperty.Register("UserStories", typeof(ObservableCollection<UserStory>), typeof(ProjectDetailWindow), new PropertyMetadata());

        public UserStory SelectedUserStory
        {
            get { return (UserStory)GetValue(SelectedUserStoryProperty); }
            set { SetValue(SelectedUserStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryProperty =
            DependencyProperty.Register("SelectedUserStory", typeof(UserStory), typeof(ProjectDetailWindow), new PropertyMetadata());


        private void OnNewSprintClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedSprint!=null)
            {
                if (SelectedSprint.Sprint_ID>0)
                {
                    MessageBox.Show("Please unselect a current row");
                }
                else
                {
                    try
                    {
                        SelectedSprint.EndDate = SelectedSprint.Date.AddDays(14);
                        modelContext.AddSprintByProjectID(proj.Project_ID, SelectedSprint);
                        MessageBox.Show("New Sprint Added");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please enter information");
            }

        }

        public ObservableCollection<Sprint> Sprints
        {
            get { return (ObservableCollection<Sprint>)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("Sprints", typeof(ObservableCollection<Sprint>), typeof(ProjectDetailWindow), new PropertyMetadata());



        public Sprint SelectedSprint
        {
            get { return (Sprint)GetValue(SelectedSprintProperty); }
            set { SetValue(SelectedSprintProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedSprint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedSprintProperty =
            DependencyProperty.Register("SelectedSprint", typeof(Sprint), typeof(ProjectDetailWindow), new PropertyMetadata());


        private void OnUpdateUserStoryClicked(object sender, RoutedEventArgs e)
        {
           //SelectedUserStory
           if (SelectedUserStory!=null)
            {
                if (SelectedUserStory.UserStory_ID>0)
                {
                    int feedBack = modelContext.UpdateUserStory(SelectedUserStory);
                    if (feedBack > 0)
                        MessageBox.Show("Successfuly updated");
                    else
                        MessageBox.Show("Nothing has been updated");
                }
            }
           else
            {
                MessageBox.Show("Please Select a row");
            }
        }


        private void OnAddUserStoryClicked(object sender, RoutedEventArgs e)
        {
            AddUserStoryWindow aWindow = new AddUserStoryWindow(SelectedSprint);
            aWindow.ShowDialog();
        }

        private void OnUpdateProjectClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                proj.Hours = int.Parse(tbHours.Text);
                proj.InitialVelocity = int.Parse(tbInitialVelocity.Text);
                proj.PivotalLink = tbPivotalLink.Text;
                proj.ProductName = tbProductName.Text;
                proj.ProjectName = tbProjectName.Text;
                proj.StartDate = Convert.ToDateTime(tbStartDate.Text);

                if (cbIsDone.IsChecked == true)
                    proj.IsDone = true;
                else
                    proj.IsDone = false;

                int updateProject = modelContext.UpdateProject(proj);
                if (updateProject > 0)
                    MessageBox.Show("Successfully updated");
                else
                    MessageBox.Show("Not updated");

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        
    }
}
