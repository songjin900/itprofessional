﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        public HomeWindow()
        {
            InitializeComponent();
        }

        private void OnRegisterBtnClicked(object sender, RoutedEventArgs e)
        {
            
            RegisterWindow rWindow = new RegisterWindow();
            rWindow.ShowDialog();
        }

        private void OnLoginBtnClicked(object sender, RoutedEventArgs e)
        {
            string email = tbEmail.Text;
            string password = tbPassword.Text;

            // we will put email for now - testing
            try
            {
                Member loginMember = modelContext.GetMemberByName(email);
                if ((loginMember.Email == email)&&(loginMember.Password ==password))
                {
                    ProjectWindow pWindow = new ProjectWindow();
                    pWindow.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Unauthorized access- Please re-enter your credential or register now!");
                }


            }
            catch(Exception ex)
            {
                MessageBox.Show("Unauthorized access- Please re-enter your credential or register now!");
                throw ex;
            }






        }

        private void OnHomeBtnClicked(object sender, RoutedEventArgs e)
        {
            //HomeWindow hWindow = new HomeWindow();
            //hWindow.Show();
            //this.Close();
        }
        private void OnProjectBtnClicked(object sender, RoutedEventArgs e)
        {
            ProjectWindow pWindow = new ProjectWindow();
            pWindow.Show();
            this.Close();
        }
        private void OnCurrentBtnClicked(object sender, RoutedEventArgs e)
        {
            CurrentWindow cWindow = new CurrentWindow();
            cWindow.Show();
            this.Close();
        }
        private void OnProductBtnClicked(object sender, RoutedEventArgs e)
        {
            BackLogWindow bWindow = new BackLogWindow();
            bWindow.Show();
            this.Close();
        }

        private void OnDashBoardBtnClicked(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dWindow = new DashBoardWindow();
            dWindow.Show();
            this.Close();
        }
        private void OnMyWorkClicked(object sender, RoutedEventArgs e)
        {
            MyWorkWindow mWindow = new MyWorkWindow();
            mWindow.ShowDialog();
        }

    }
}
