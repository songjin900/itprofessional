﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        public RegisterWindow()
        {
            InitializeComponent();
        }
        private void OnRegisterBtnClicked(object sender, RoutedEventArgs e)
        {
            // Todo: Register User here
            // open connection here with database and add.
            Member newMember = new Member();
            newMember.Name = tbregisterName.Text;
            newMember.Email = tbregisterEmail.Text;
            newMember.Password = tbregisterPassword.Text;
            if (cbregisterIsPM.IsChecked == true)
                newMember.IsPM = true;
            else
                newMember.IsPM = false;

            if (cbregisterTeam.SelectedValue == "DevTeam1")
                newMember.Team_ID = 1;
            else if (cbregisterTeam.SelectedValue == "DevTeam2")
                newMember.Team_ID = 2;
            else if (cbregisterTeam.SelectedValue == "DevTeam3")
                newMember.Team_ID = 3;

            int i=modelContext.AddMember(newMember);
            if (i>0)
            {
                MessageBox.Show("Account Created");
            }
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // province
            this.cbregisterTeam.Items.Add("DevTeam1");
            this.cbregisterTeam.Items.Add("DevTeam2");
            this.cbregisterTeam.Items.Add("DevTeam3");
        }

    }
}
