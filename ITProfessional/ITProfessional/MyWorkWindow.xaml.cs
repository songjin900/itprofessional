﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using ITProfessional.Models;


using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for MyWorkWindow.xaml
    /// </summary>
    public partial class MyWorkWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        public MyWorkWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Member> memberdb = modelContext.GetAllMember();
                ObservableCollection<Member> temp = new ObservableCollection<Member>();
                foreach (var m in memberdb)
                {
                    temp.Add(m);
                }

                Members = temp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public ObservableCollection<Member> Members
        {
            get { return (ObservableCollection<Member>)GetValue(MembersProperty); }
            set { SetValue(MembersProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Members.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MembersProperty =
            DependencyProperty.Register("Members", typeof(ObservableCollection<Member>), typeof(MyWorkWindow), new PropertyMetadata());



        public Member SelectedMember
        {
            get { return (Member)GetValue(SelectedMemberProperty); }
            set { SetValue(SelectedMemberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedMember.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedMemberProperty =
            DependencyProperty.Register("SelectedMember", typeof(Member), typeof(MyWorkWindow), new PropertyMetadata());


        //OnSelectBtnClicked
        private void OnSelectBtnClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SelectedMember !=null)
                {
                    if(SelectedMember.Member_ID>0)
                    {


                        List<UserStory> userstoryDB = modelContext.GetAllUserStoryByMemberID(SelectedMember.Member_ID);
                        ObservableCollection<UserStory> temp3 = new ObservableCollection<UserStory>();
                        foreach (var m in userstoryDB)
                        {
                            temp3.Add(m);
                        }

                        userStories = temp3;
                    }
                }
                else
                {
                    MessageBox.Show("Please select a row");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public ObservableCollection<UserStory> userStories
        {
            get { return (ObservableCollection<UserStory>)GetValue(userStoriesProperty); }
            set { SetValue(userStoriesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for userStories.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty userStoriesProperty =
            DependencyProperty.Register("userStories", typeof(ObservableCollection<UserStory>), typeof(MyWorkWindow), new PropertyMetadata());



        public UserStory SelectedUserStory
        {
            get { return (UserStory)GetValue(SelectedUserStoryProperty); }
            set { SetValue(SelectedUserStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryProperty =
            DependencyProperty.Register("SelectedUserStory", typeof(UserStory), typeof(MyWorkWindow), new PropertyMetadata());


        DateTime start = new DateTime();
        DateTime end = new DateTime();
        int AutoHours;

        private void OnStartBtnClicked(object sender, RoutedEventArgs e)
        {
            var startTime = DateTime.Now;
            tbStart.Text = startTime.ToString();

            start = startTime;
        }

        private void OnEndBtnClicked(object sender, RoutedEventArgs e)
        {
            var endTime = DateTime.Now;
            tbFinish.Text = endTime.ToString();
            end = endTime;

            start = Convert.ToDateTime(tbStart.Text);

            var sHour = Convert.ToDouble(end.Hour) - Convert.ToDouble(start.Hour);
            var sMin = Convert.ToDouble(end.Minute) - Convert.ToDouble(start.Minute);
            AutoHours = (int)sHour;
           
            tbTotal.Text = sHour.ToString()+"hrs "+sMin.ToString()+"mins";



        }//UpdateUserStory

        private void OnUpdateClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SelectedUserStory!=null)
                {
                    if (SelectedUserStory.UserStory_ID>0)
                    {
                        // case: auto only exists
                        // case: manual only exists
                        // case: all exists;
                        if ((tbTotal.Text.Length>0) && (tbManualHrs.Text.Length==0))
                        {
                            SelectedUserStory.TotalHours += (AutoHours);
                        }
                        else if ((tbTotal.Text.Length==0)&&(tbManualHrs.Text.Length>0))
                        {
                            if (int.Parse(tbManualHrs.Text) > 0)
                            {
                                SelectedUserStory.TotalHours += (int.Parse(tbManualHrs.Text));
                            }
                        }
                        else
                        {
                            if (tbManualHrs.Text.Length>0)
                            {
                              SelectedUserStory.TotalHours += (int.Parse(tbManualHrs.Text));

                            }
                            
                            
                        }
                        
                        int feedback = modelContext.UpdateUserStory(SelectedUserStory);
                        if (feedback > 0)
                            MessageBox.Show("Successfuly updated");
                        else
                            MessageBox.Show("Nothing has been changed");
                    }
                }
                else
                {
                    MessageBox.Show("Please select a row");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }//UpdateUserStory



    }
}
