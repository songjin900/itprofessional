﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{
    public class SprintReport
    {
        [Key]
        public int SprintReport_ID { get; set; }
        public string Status { get; set; }
        public int InitialEstimate { get; set; }
        public int ReEstimates { get; set; }
        public int TimeSpents { get; set; }
    }
}
