﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{

    public class UserStory
    {
        [Key]
        public int UserStory_ID { get; set; }
        public string Title { get; set; }
        public int StoryPoint { get; set; }
        public int InitialEstimates { get; set; }

        public int? ReEstimate { get; set; }
        public string Status { get; set; }
        public int TotalHours { get; set; }
        public bool IsDone { get; set; }
        public string MemberName { get; set; }
        public int? Sprint_ID { get; set; }

        //[ForeignKey("Member_ID")]
        // MAKE THIS Nullable if you can

        public int? Member_ID { get; set; }
        public Member Member { get; set; }

        ////[ForeignKey("Project_ID")]
        //public int Project_ID { get; set; }
        //public Project Project { get; set; }


        // this has to be nullable such that
        // public int? Sprint_ID {get;set;}

        public Sprint Sprint { get; set; }

        
    }
}
