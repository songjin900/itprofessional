﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{

    public class Team
    {
        [Key]
        public int Team_ID { get; set; }
        public string TeamName { get; set; }

      //  public ICollection<Project> Projects { get; set; }
        public ICollection<Member> Members { get; set; }
    }
}
