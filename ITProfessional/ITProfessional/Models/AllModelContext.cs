﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace ITProfessional.Models
{
    public class AllModelContext
    {
        public Member GetMemberByName(string email)
        {
            Member selectedMember = null;
            try
            {
                ProjectDBContext pContext = new ProjectDBContext();
                selectedMember = pContext.Members.FirstOrDefault(m => m.Email == email);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting member");
                throw ex;
            }
            return selectedMember;
        }
        public Member GetMemberByID(int id)
        {
            Member selectedMember = null;
            try
            {
                ProjectDBContext pContext = new ProjectDBContext();
                selectedMember = pContext.Members.FirstOrDefault(m => m.Member_ID == id);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting member");
                throw ex;
            }
            return selectedMember;
        }

        public Project GetProjectByProjectID(int id)
        {
            Project selectedProject = null;
            try
            {
                ProjectDBContext pContext = new ProjectDBContext();
                selectedProject = pContext.Projects.Where(p => p.Project_ID == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting selectedProject");
                throw ex;
            }
            return selectedProject;
        }

        public int AddMember (Member newMember)
        {
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                hpd.Members.Add(newMember);
                hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting selectedProject");
                throw ex;
            }

            return newMember.Member_ID;
        }

        public int DeleteMember(int id)
        {
            int memberDeleted = -1;

            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                Member deleteMember = hpd.Members.FirstOrDefault(m => m.Member_ID == id);
                hpd.Members.Remove(deleteMember);
                memberDeleted = hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Problem in " + GetType().Name + " " + MethodBase.GetCurrentMethod().Name + " " + ex.Message);
                throw ex;
            }

            return memberDeleted;
        }

        public int Update(Member member)
        {
            int memberUpdated = -1;

            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                Member updateMember = hpd.Members.FirstOrDefault(emp => emp.Member_ID == member.Member_ID);
                hpd.Entry(updateMember).CurrentValues.SetValues(member);
                memberUpdated = hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in updating member");
               
                throw ex;
            }

            return memberUpdated;
        }

        public List<Project> GetAllProjects()
        {
            List<Project> allProjects = new List<Project>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allProjects = hpd.Projects.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allProjects;
        }

  

        public List<Sprint> GetSprintByProjectID(int projectid)
        {
            List<Sprint> allSprints = new List<Sprint>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allSprints = hpd.Sprints.Where(s=>s.Project_ID.Equals(projectid)).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allSprints;

        }



        public List<UserStory> GetAllUserStoryCurrent()
        {
           // List<Sprint> allSprint = GetSprintByProjectID(1);

            List<UserStory> allUserStories = new List<UserStory>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allUserStories = hpd.UserStories.Where(u=>u.Sprint_ID!=null).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allUserStories;
        }


        public List<UserStory> GetAllUserStoryBacklog()
        {
            // List<Sprint> allSprint = GetSprintByProjectID(1);

            List<UserStory> allUserStories = new List<UserStory>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allUserStories = hpd.UserStories.Where(u=>u.Sprint_ID== null).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allUserStories;
        }

        public List<UserStory> GetAllUserStoryBySprintID(int sprintId)
        {
            //  allSprints = hpd.Sprints.Where(s=>s.Project_ID.Equals(projectid)).ToList();


            List<UserStory> allUserStories = new List<UserStory>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allUserStories = hpd.UserStories.Where(s => s.Sprint_ID.Equals(sprintId)).ToList();

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allUserStories;
        }

        public List<UserStory> GetAllUserStoryByMemberID(int memberid)
        {

            List<UserStory> allUserStories = new List<UserStory>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allUserStories = hpd.UserStories.Where(s => s.Member_ID.Equals(memberid)).ToList();

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allUserStories;
        }

        public int AddUserStory(UserStory us)
        {
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                hpd.UserStories.Add(us);
                hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting selectedProject");
                throw ex;
            }

            return us.UserStory_ID;
        }

        //
        public int UpdateUserStory(UserStory us)
        {
            int usUpdated = -1;

            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                UserStory updateUS = hpd.UserStories.FirstOrDefault(u => u.UserStory_ID == us.UserStory_ID);
                hpd.Entry(updateUS).CurrentValues.SetValues(us);
                usUpdated = hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in updating member");

                throw ex;
            }

            return usUpdated;
        }



        public int UpdateUserStoryBySprintID(UserStory currentUserStory)
        {
            int userStoryUpdated = -1;

            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                UserStory updateUS = hpd.UserStories.FirstOrDefault(u => u.Sprint_ID == currentUserStory.Sprint_ID);
                var temp = currentUserStory.UserStory_ID;
                hpd.Entry(updateUS).CurrentValues.SetValues(currentUserStory);
                userStoryUpdated = hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in updating member");

                throw ex;
            }

            return userStoryUpdated;
        }
        public void AddProject(Project newProject)
        {
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                hpd.Projects.Add(newProject);
                hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting selectedProject");
                throw ex;
            }
        }

        public void AddSprintByProjectID(int projectID, Sprint selectedSprint)
        {
            selectedSprint.Project_ID = projectID;
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                hpd.Sprints.Add(selectedSprint);
                hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem with getting selectedProject");
                throw ex;
            }
        }

        // bring all projects
        // brint all
        public CustomDashBoardSprint GetSingleTimesBySprintID(int sprintid)
        {
            CustomDashBoardSprint cp = new CustomDashBoardSprint();
            int EstimateTimes = 0;
            int ActualTimes = 0;

            try
            {
              List<UserStory> uslist = GetAllUserStoryBySprintID(sprintid);
                foreach (var u in uslist)
                {
                    EstimateTimes += u.InitialEstimates;
                    ActualTimes += u.TotalHours;
                }
                cp.Sprint_ID = sprintid;
                cp.InitialEstimates =(EstimateTimes);
                cp.ActualTimes = ActualTimes;
                double et = EstimateTimes;
                double at = ActualTimes;
                cp.Percentage = Math.Round((et / at) * 100, 2);

                return cp;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CustomDashBoardSprint> GetAllTimeBySprintByProjectID(int projectid)
        {
            try
            {
                List<Sprint> sprintList = GetSprintByProjectID(projectid);
                List<CustomDashBoardSprint> cSprintList = new List<CustomDashBoardSprint>();
                foreach (var s in sprintList) 
                {
                    cSprintList.Add(GetSingleTimesBySprintID(s.Sprint_ID));
                }
                return cSprintList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CustomDashBoardProject> GetAllTimeByProject()
        {
            int EstimateTimes = 0;
            int ActualTimes = 0;
            try
            {
                List<CustomDashBoardProject> customProject = new List<CustomDashBoardProject>();
                List<Project> allProject = GetAllProjects();
                foreach (var p in allProject)
                {
                    List<CustomDashBoardSprint> customSprint = GetAllTimeBySprintByProjectID(p.Project_ID);
                    CustomDashBoardProject cdbp = new CustomDashBoardProject();
                    foreach (var s in customSprint)
                    {
                        EstimateTimes += s.InitialEstimates;
                        ActualTimes += s.ActualTimes;
                    }
                    cdbp.Project_ID = p.Project_ID;
                    cdbp.InitialEstimates = EstimateTimes;
                    cdbp.ActualTimes = ActualTimes;
                    double et = EstimateTimes;
                    double at = ActualTimes;
                    cdbp.Percentage = Math.Round((et / at) * 100, 2);

                    customProject.Add(cdbp);


                }
                return customProject;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public int UpdateProject(Project project)
        {
            int feedback = -1;
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                Project updateProject = hpd.Projects.FirstOrDefault(p => p.Project_ID == project.Project_ID);
                hpd.Entry(updateProject).CurrentValues.SetValues(project);
                feedback = hpd.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in updating");

                throw ex;
            }
            return feedback;

        }

        public List<Member> GetAllMember()
        {
            List<Member> allMembers = new List<Member>();
            try
            {
                ProjectDBContext hpd = new ProjectDBContext();
                allMembers = hpd.Members.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error in Getting all projects");

                throw ex;
            }
            return allMembers;
        }


    }
}
