﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{


    public class ProjectDBContext : DbContext
    {


        public  DbSet<Team> Teams { get; set; }
        public  DbSet<Project> Projects { get; set; }
        public  DbSet<Member> Members { get; set; }
        public  DbSet<Sprint> Sprints { get; set; }
        public  DbSet<UserStory> UserStories { get; set; }
        public DbSet<SprintReport> SprintReports  { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=info3112db.database.windows.net;Initial Catalog=ITProfessionaldb;Persist Security Info=True;User ID=songjin900;Password=110490Sj!");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            //modelBuilder.Entity<Post>()
            // .HasOne(p => p.Blog)
            //.WithMany(b => b.Posts)
            // .HasForeignKey(p => p.BlogId)
            // .HasConstraintName("ForeignKey_Post_Blog");

            modelBuilder.Entity<Member>()
                .HasOne(m => m.Team)
                .WithMany(b => b.Members)
                .HasForeignKey(p => p.Team_ID);

            //modelBuilder.Entity<Project>()
            //   .HasOne(m => m.Team)
            //   .WithMany(b => b.Projects)
            //   .HasForeignKey(p => p.Team_ID);

            modelBuilder.Entity<Sprint>()
               .HasOne(m => m.Project)
               .WithMany(b => b.Sprints)
               .HasForeignKey(p => p.Project_ID);

            modelBuilder.Entity<UserStory>()
               .HasOne(m => m.Member)
               .WithMany(b => b.UserStory)
               .HasForeignKey(p => p.Member_ID);

            //wrong
            //modelBuilder.Entity<UserStory>()
            //     .HasOne(m => m.Project)
            //     .WithMany(b => b.UserStorys)
            //     .HasForeignKey(p => p.Project_ID);

            modelBuilder.Entity<UserStory>()
                 .HasOne(m => m.Sprint)
                 .WithMany(b => b.UserStory)
                 .HasForeignKey(p => p.Sprint_ID);








        }

    }
}
