﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{
    public class Project
    {
        [Key]
        public int Project_ID { get; set; }
        public string ProductName { get; set; }
        public string ProjectName { get; set; }
        public int InitialVelocity { get; set; }
        public int Hours { get; set; }
        public string PivotalLink { get; set; }
        public DateTime StartDate { get; set; }

        public bool IsDone { get; set; }

        public ICollection<Sprint> Sprints { get; set; }
        //public ICollection<UserStory> UserStorys { get; set; }

        //[ForeignKey("Team_ID")]
        //public int Team_ID { get; set; }
        //public Team Team { get; set; }
    }
}
