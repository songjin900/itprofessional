﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{
    public class CustomDashBoardProject
    {
        public int Project_ID { get; set; }
        public int InitialEstimates { get; set; }
        public int ActualTimes { get; set; }
        public double Percentage { get; set; }
    }
}
