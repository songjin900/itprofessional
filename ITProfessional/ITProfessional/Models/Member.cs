﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ITProfessional.Models
{
    public class Member
    {
        [Key]
        public int Member_ID { get; set; }
        public bool IsPM { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public ICollection<UserStory> UserStory { get; set; }

        //[ForeignKey("Team_ID")]

        //make it nullable if you can 
        public int? Team_ID { get; set; }
        public Team Team { get; set; }



    }

}

//Install-Package Microsoft.AspNetCore.Mvc.DataAnnotations -Version 2.2.0