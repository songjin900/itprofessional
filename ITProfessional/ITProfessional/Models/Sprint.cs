﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITProfessional.Models
{
    public class Sprint
    {
        [Key]
        public int Sprint_ID { get; set; }

        public string Name { get; set; }
        
        public DateTime Date { get; set; } // need to rename to StartDate

        public DateTime? EndDate { get; set; }

        public bool IsDone { get; set; }

        public ICollection<UserStory> UserStory { get; set; }

        //[ForeignKey("Project_ID")]
        public int Project_ID { get; set; }
        public Project Project { get; set; }
    }
}
