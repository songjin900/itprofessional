﻿using ITProfessional.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ITProfessional
{
    /// <summary>
    /// Interaction logic for AddUserStoryWindow.xaml
    /// </summary>
    public partial class AddUserStoryWindow : Window
    {
        AllModelContext modelContext = new AllModelContext();

        UserStory currentUserStory = new UserStory();
        Sprint sprint = new Sprint();
        
        // new 
        public AddUserStoryWindow()
        {
            InitializeComponent();
            GBSelectFromBackLog.IsEnabled = false;
            AddBtnFromGrid.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            btnAdd.IsEnabled = true;
            this.DataContext = this;
        }

        // new 2
        public AddUserStoryWindow(Sprint sp)
        {
            InitializeComponent();
            btnUpdate.IsEnabled = false;
            btnAdd.IsEnabled = true;
            // i need sp.sprint_id;
            tbSprintID.Text = sp.Sprint_ID.ToString();
            sprint = sp;
            this.DataContext = this;

        }

        //update
        public AddUserStoryWindow(UserStory us, Sprint sprint)
        {
            InitializeComponent();
            btnUpdate.IsEnabled = true;
            btnAdd.IsEnabled = false;
            tbTitle.Text = us.Title.ToString() ;
            tbUSStoryPoint.Text = us.StoryPoint.ToString();
            tbUserToryID.Text = us.UserStory_ID.ToString();
            tbUSInitialEstimatedTime.Text = us.InitialEstimates.ToString();
            tbStatus.Text = us.Status;
            tbMemberID.Text = us.Member_ID.ToString();
            tbSprintID.Text = us.Sprint_ID.ToString();
            tbTotalHours.Text = us.TotalHours.ToString();
            tbReEstimate.Text = us.ReEstimate.ToString();
            currentUserStory = us;
            this.sprint = sprint;
        }



        private void OnAddClicked(object sender, RoutedEventArgs e)
        {
            UserStory s = new UserStory();
            s.Title = tbTitle.Text.ToString();
            s.StoryPoint = int.Parse(tbUSStoryPoint.Text) ;
            s.InitialEstimates = int.Parse(tbUSInitialEstimatedTime.Text);
            s.TotalHours = int.Parse(tbTotalHours.Text);
            s.Status = tbStatus.Text.ToString();
            if (sprint.Sprint_ID>0)
                s.Sprint_ID = sprint.Sprint_ID;
            else
                s.Sprint_ID = null;

            if (tbMemberID.Text.Length>0)
                s.Member_ID = int.Parse(tbMemberID.Text);
            else
                s.Member_ID = null;
               
           
            try
            {

                int isAdded = modelContext.AddUserStory(s);
                if (isAdded > 0)
                {
                    MessageBox.Show("UserStory added");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("error in adding UserStory");
                throw ex;

            }
           
        }
        //OnAddFromGridClicked

        private void OnAddFromGridClicked(object sender, RoutedEventArgs e)
        {
            if (SelectedUserStory.UserStory_ID>0)
            {
                SelectedUserStory.Sprint_ID = sprint.Sprint_ID;

                try
                {

                    int isAdded = modelContext.UpdateUserStory(SelectedUserStory);
                    if (isAdded > 0)
                    {
                        MessageBox.Show("UserStory Added");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error in adding UserStory");
                    throw ex;

                }
                
            }
            else
            {
                MessageBox.Show("Please Click a Row");
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<UserStory> userstoryDB = modelContext.GetAllUserStoryBacklog();
                ObservableCollection<UserStory> temp = new ObservableCollection<UserStory>();
                foreach (var u in userstoryDB)
                {
                    temp.Add(u);
                }

                UserStories = temp;

            }
            catch (Exception ex)
            {
                Console.WriteLine("error in loading up CB " + ex);
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }

        public ObservableCollection<UserStory> UserStories
        {
            get { return (ObservableCollection<UserStory>)GetValue(MyPropertyProperty); }
            set { SetValue(MyPropertyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("UserStories", typeof(ObservableCollection<UserStory>), typeof(AddUserStoryWindow), new PropertyMetadata());

        public UserStory SelectedUserStory
        {
            get { return (UserStory)GetValue(SelectedUserStoryProperty); }
            set { SetValue(SelectedUserStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedUserStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserStoryProperty =
            DependencyProperty.Register("SelectedUserStory", typeof(UserStory), typeof(AddUserStoryWindow), new PropertyMetadata());



        //



















        //// Do Not delete this
        //private void OnUpdateClicked(object sender, RoutedEventArgs e)
        //{
        //    int originalTime = currentUserStory.TotalHours;
        //    string originalString = tbTotalHr.ToString();


        //    currentUserStory.TotalHours = 12;
        //    MessageBox.Show("Todo: time field is also problem +if i update this twice in a row, it gives an error");
        //    try
        //    {

        //        int isUpdated = modelContext.UpdateUserStoryBySprintID(currentUserStory);

        //        CurrentWindow cWindow =  new CurrentWindow(sprint);
        //        cWindow.Show();
        //        this.Close();


        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("error in adding UserStory");
        //        throw ex;

        //    }
        //}

        //DateTime start = new DateTime();
        //DateTime end = new DateTime();

        //private void OnStartBtnClicked(object sender, RoutedEventArgs e)
        //{
        //    var startTime = DateTime.Now;
        //    tbTimeStart.Text = startTime.ToString();
        //    //  MessageBox.Show(startTime.Hour.ToString());
        //    //  tbTimeStart.Text = startTime.Hour.ToString() + " " + startTime.Minute.ToString(); ;
        //    btnStart.IsEnabled = false;
        //    btnEnd.IsEnabled = true;

        //    start = startTime;
        //}

        //private void OnEndBtnClicked(object sender, RoutedEventArgs e)
        //{
        //    btnStart.IsEnabled = false;
        //    var endTime = DateTime.Now;
        //    tbTimeEnd.Text = endTime.ToString();
        //    //  tbTimeEnd.Text = endTime.Hour.ToString() + " " + endTime.Minute.ToString(); 

        //    end = endTime;

        //    var sHour = Convert.ToDouble(end.Hour) - Convert.ToDouble(start.Hour);
        //    var sMin = Convert.ToDouble(end.Minute) - Convert.ToDouble(start.Minute);

        //    if (sMin >= 30)
        //        sHour += 1;



        //    MessageBox.Show("Todo: Estimation of hours");
        //    tbTotalHr.Text = sHour.ToString();

        //    //currentUserStory.UserStory_ID


        //}


    }

    //OnAddClicked
}
